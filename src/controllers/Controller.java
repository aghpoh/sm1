package controllers;

import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.effect.BoxBlur;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.Reflection;
import javafx.scene.image.*;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;

import javafx.scene.canvas.Canvas;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.imageio.ImageIO;

import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class Controller implements Initializable {

    @FXML
    private Slider contrastSlider;
    @FXML
    private Slider luminositySlider;
    @FXML
    private Slider saturationSlider;
    @FXML
    private Canvas canvas;
    @FXML
    private Slider blurSlider;
    @FXML
    private ColorPicker colorPicker;

    private Stage thisStage;
    private Scene scene;
    private GraphicsContext gc;

    public void setStage(Stage stage) {
        thisStage = stage;
    }

    private double contrast = 0;
    private double brightness = 0;
    private double saturation = 0;
    private File file;
    private File fileEdit;
    private ColorAdjust colorAdjust = new ColorAdjust();

    public void setFile(File file) {
        this.file = file;
    }

    @FXML
    private void fileChooser() {
        FileChooser fileChooser = new FileChooser();
        file = fileChooser.showOpenDialog(new Stage());
        try {
            imageCanvas();
        } catch (IOException ex) {
        }

    }

    @FXML
    private void exitAction() {
        File file = new File("/home/pit/Desktop/image.png");
        file.delete();
        Platform.exit();
        System.exit(0);
    }

    @FXML
    private void saveAction() {
        saveFile();
    }

    private void imageCanvas() throws IOException {
        BufferedImage bufferedImage = ImageIO.read(file);
        Image image = SwingFXUtils.toFXImage(bufferedImage, null);
        canvas.setWidth(image.getWidth());
        canvas.setHeight(image.getHeight());
        init();
        gc.drawImage(image, 0, 0, image.getWidth(), image.getHeight());
    }

    @FXML
    private void cropImageAction(ActionEvent event) throws Exception {

        Stage stageq = (Stage) canvas.getScene().getWindow();
        stageq.close();

        File file = new File("/home/pit/Desktop/image.png");

        WritableImage writableImage = new WritableImage((int) canvas.getWidth(), (int) canvas.getHeight());
        canvas.snapshot(null, writableImage);
        RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
        ImageIO.write(renderedImage, "png", file);

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../resources/view/crop.fxml"));
        Stage stage = new Stage();
        stage.setTitle("ABC");
        Parent root = (Parent) fxmlLoader.load();
        ModalController controller = fxmlLoader.getController();

        controller.setFile(file);
        controller.setThisStage(thisStage);
        controller.setScene(scene);

        stage.setScene(new Scene(root));
        stage.show();

    }

    private File saveFile() {
        FileChooser fileChooser = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilter =
                new FileChooser.ExtensionFilter("png files (*.png)", "*.png");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File file = fileChooser.showSaveDialog(thisStage);

        if (file != null) {
            try {
                WritableImage writableImage = new WritableImage((int) canvas.getWidth(), (int) canvas.getHeight());
                canvas.snapshot(null, writableImage);
                RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
                ImageIO.write(renderedImage, "png", file);
            } catch (IOException ex) {
            }
        }
        return file;
    }

    @FXML
    private void rotateImage() {
        if (canvas != null) {
            canvas.setRotate(canvas.getRotate() + 90);

            canvas.setWidth(canvas.getWidth());
            canvas.setHeight(canvas.getHeight());

        }
    }

    @FXML
    private void greyEffect() {
        colorAdjust.setSaturation(-1);
        canvas.setEffect(colorAdjust);
    }

    @FXML
    private void drawImage() throws IOException {


    }

    @FXML
    private void scaleImage() {

        canvas.setScaleX(2);
        canvas.setScaleY(2);

    }

    @FXML
    private void mirrorEffect() {
        Reflection reflection = new Reflection();
        reflection.setInput(colorAdjust);
        reflection.setBottomOpacity(1);
        reflection.setTopOffset(25);
        reflection.setTopOpacity(1);
        canvas.setEffect(reflection);
    }

    @FXML
    private void invertEffect() throws IOException {
        BufferedImage bufferedImage = ImageIO.read(file);
        Image image2 = SwingFXUtils.toFXImage(bufferedImage, null);
        PixelReader pixelReader = image2.getPixelReader();
        WritableImage dest
                = new WritableImage(
                (int) image2.getWidth(),
                (int) image2.getHeight());
        PixelWriter pixelWriter = dest.getPixelWriter();

        for (int y = 0; y < image2.getHeight(); y++) {
            for (int x = 0; x < image2.getWidth(); x++) {
                Color color = pixelReader.getColor(x, y);
                color = color.invert();
                pixelWriter.setColor(x, y, color);
            }
        }
        canvas.setWidth(dest.getWidth());
        canvas.setHeight(dest.getHeight());
        gc.drawImage(dest, 0, 0, dest.getWidth(), dest.getHeight());
//        imgEdit.setImage(dest);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        fileEdit = new File("/home/pit/Desktop/image.png");
        if (fileEdit.exists()) {
            try {
                file = fileEdit;
                imageCanvas();
                init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void imgSetEffect(double contrast, double brightness, double saturation) {
        colorAdjust.setContrast(contrast);
        colorAdjust.setBrightness(brightness);
        colorAdjust.setSaturation(saturation);
        canvas.setEffect(colorAdjust);
    }

    private void sliderSetDefaultValue(int i, int i1, Slider contrastSlider) {
        contrastSlider.setMin(-1);
        contrastSlider.setMax(1);
    }

    private void init() {

        gc = canvas.getGraphicsContext2D();
        canvas.setOnMousePressed((event) -> {
            gc.beginPath();
            gc.moveTo(event.getX(), event.getY());
            gc.stroke();
        });

        canvas.setOnMouseDragged(event -> {
            gc.setFill(colorPicker.getValue());
            gc.fillRect(event.getX(), event.getY(), 5, 5);

        });


        sliderSetDefaultValue(-1, 1, contrastSlider);
        sliderSetDefaultValue(-1, 1, luminositySlider);
        sliderSetDefaultValue(-1, 1, saturationSlider);

        contrastSlider.valueProperty().addListener((observable, oldValue, newValue) -> {

            contrast = newValue.doubleValue();
            imgSetEffect(contrast, brightness, saturation);

        });
        luminositySlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            brightness = newValue.doubleValue();
            imgSetEffect(contrast, brightness, saturation);

        });
        saturationSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            saturation = newValue.doubleValue();
            imgSetEffect(contrast, brightness, saturation);

        });
        blurSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            BoxBlur bb = new BoxBlur();
            bb.setWidth(newValue.doubleValue());
            bb.setHeight(newValue.doubleValue());
            bb.setInput(colorAdjust);
            canvas.setEffect(bb);

        });
    }
}



