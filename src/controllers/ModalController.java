package controllers;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by pit on 3/5/17.
 */
public class ModalController {
    @FXML
    private TextField width;
    @FXML
    private TextField height;

    private File file;
    private Canvas canvas = new Canvas();

    Stage thisStage;
    Scene scene;

    public void setThisStage(Stage thisStage) {
        this.thisStage = thisStage;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    @FXML
    private void cropAction(ActionEvent event) throws IOException {

        BufferedImage bufferedImage = ImageIO.read(file);
        Image image = SwingFXUtils.toFXImage(bufferedImage, null);
        canvas.setWidth(image.getWidth());
        canvas.setHeight(image.getHeight());
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.drawImage(image, 0, 0, image.getWidth(), image.getHeight());

        WritableImage writableImage = new WritableImage(Integer.parseInt(width.getText()), Integer.parseInt(height.getText()));
        canvas.snapshot(null, writableImage);
        RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
        ImageIO.write(renderedImage, "png", file);

        Stage stage1 = (Stage) width.getScene().getWindow();
        stage1.close();

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../resources/view/sample.fxml"));
        Stage stage = new Stage();
        stage.setTitle("MAIN");
        Parent root = (Parent) fxmlLoader.load();
        Controller controller = new Controller();
        controller.setFile(file);
        fxmlLoader.setController(controller);
        stage.setScene(new Scene(root));
        stage.show();

    }


    public void setFile(File file) {
        this.file = file;
    }
}
